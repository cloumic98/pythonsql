from utils.SqlLib import SqlClient
from utils.Type import Type


class Pokemon:

    def __init__(self, info):
        self.num = info[0]
        self.name = info[1]
        self.height = info[2]
        self.weight = info[3]
        self.is_starter = True if info[4] == 1 else False
        self.type = Pokemon.get_type(self.num)

    def to_string(self):
        result = "Pokedex n° : " + str(self.num) + ",\n"
        result += "Name : " + self.name + ",\n"
        result += "Height : " + str(self.height) + ",\n"
        result += "Weight : " + str(self.weight) + ",\n"
        result += "Type 1 : " + self.type[0].name + ",\n"
        if len(self.type) == 2:
            result += "Type 2 : " + self.type[1].name + ",\n"
        result += "Is a starter : " + str(self.is_starter)
        return result

    @staticmethod
    def get_pokemon_by_id(pokemon_id):
        req = "SELECT * FROM pokemon WHERE idPokemon = " + str(pokemon_id) + ";"
        data = SqlClient.get_instance().request_one(req)
        return None if data is None else Pokemon(data)

    @staticmethod
    def get_pokemon_by_name(pokemon_name):
        req = "SELECT * FROM pokemon WHERE name LIKE '" + pokemon_name + "';"
        data = SqlClient.get_instance().request_one(req)
        return None if data is None else Pokemon(data)

    @staticmethod
    def get_type(num_pokemon):
        response = []
        req = "SELECT type.idType, type.name FROM pokemon JOIN pokemon_has_type ON pokemon.idpokemon = pokemon_has_type.pokemon_idpokemon"
        req += " JOIN type ON type.idtype = pokemon_has_type.type_idtype WHERE pokemon.idpokemon = " + str(num_pokemon) + ";"
        data = SqlClient.get_instance().request_all(req)
        response.append(Type(data[0][0], data[0][1]))
        if len(data) == 2:
            response.append(Type(data[1][0], data[1][1]))
        return response
