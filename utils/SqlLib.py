import mysql.connector


class SqlClient:

    sql_client = None

    def __init__(self):
        self.host = "localhost"
        self.port = "3306"
        self.db_name = "pokemon"
        self.user = "root"
        self.password = ""
        self.connection = None

    def get_connection(self):
        if self.connection is None or not self.connection.is_connected():
            self.connection = mysql.connector.connect(user=self.user, password=self.password, host=self.host,
                                                      port=self.port,
                                                      database=self.db_name)
        return self.connection

    def request_one(self, sql):
        cursor = self.get_connection().cursor()
        cursor.execute(sql)
        response = cursor.fetchone()
        cursor.close()
        return response

    def request_all(self, sql):
        cursor = self.get_connection().cursor()
        cursor.execute(sql)
        response = cursor.fetchall()
        cursor.close()
        return response

    @staticmethod
    def get_instance():
        if SqlClient.sql_client is None:
            SqlClient.sql_client = SqlClient()
        return SqlClient.sql_client
