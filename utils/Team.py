from utils.SqlLib import SqlClient
from utils.Pokemon import Pokemon


class Team:

    def __init__(self, name, team):
        self.name = name
        self.team = []
        for s in team.split(","):
            self.add_pokemon(Pokemon.get_pokemon_by_id(s))

    def get_pokemon(self, index):
        return self.team[index]

    def add_pokemon(self, pokemon_add):
        self.team.append(pokemon_add)

    def to_string(self):
        result = "Team name : " + self.name + ",\n"
        result += "Team pokemon : \n"
        for pkmn in self.team:
            result += " - " + pkmn.name + "\n"
        return result

    @staticmethod
    def get_all_team():
        response = []
        req = "SELECT team.teamName, GROUP_CONCAT(pokemon.idpokemon) FROM team JOIN team_has_pokemon"
        req += " ON team.idteam = team_has_pokemon.team_idteam JOIN pokemon ON team_has_pokemon.pokemon_idpokemon = pokemon.idpokemon"
        req += " GROUP BY team.idteam;"
        answer = SqlClient.get_instance().request_all(req)
        for data in answer:
            response.append(Team(data[0], data[1]))
        return response
