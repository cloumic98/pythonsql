from utils.Pokemon import Pokemon
from utils.Team import Team

running = True

if __name__ == '__main__':
    while running:
        print("________________________________")
        print("1 - print pokemon info by id")
        print("2 - print pokemon info by name")
        print("3 - display all teams registered")
        print("4 - register a team")
        print("5 - exit program")
        print("________________________________")
        choice = int(input("Make a choice : "))
        print("________________________________")
        if choice == 1:
            pkmn_id = int(input("Select an pokemon by id (1>= id <= 1118) : "))
            pkmn = Pokemon.get_pokemon_by_id(pkmn_id)
            if pkmn is not None:
                print(pkmn.to_string())
            else:
                print("No pokemon for " + pkmn_name)
        elif choice == 2:
            pkmn_name = input("Select an pokemon by name : ")
            pkmn = Pokemon.get_pokemon_by_name(pkmn_name)
            if pkmn is not None:
                print(pkmn.to_string())
            else:
                print("No pokemon for " + pkmn_name)
        elif choice == 3:
            teams = Team.get_all_team()
            for team in teams:
                print(team.to_string())
            if not teams:
                print("No team")
        elif choice == 4:
            team_name = input("Entre team name : ")
            pokemon_list = []
            while len(pokemon_list) != 6:
                res = input("Select pokemon by name or id (1>= id <= 1118) : ")
                try:
                    value = int(res)
                    pokemon_list.append(str(Pokemon.get_pokemon_by_id(value).num))
                except ValueError:
                    value = res
                    pokemon_list.append(str(Pokemon.get_pokemon_by_name(value).num))
            team = Team(team_name, ",".join(pokemon_list))
            print(team.to_string())
        elif choice == 5:
            running = False

        input("continue (press enter)")
